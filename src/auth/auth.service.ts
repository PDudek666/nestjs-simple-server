import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { UserInterface } from '../user/user.interface';
import { KeyObject } from 'crypto';
import { jwtSecret } from '../config';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  login(user: UserInterface): { authToken: string } {
    const foundUser = this.userService.find(user.email);
    if (!foundUser || foundUser.password !== user.password) {
      throw new UnauthorizedException();
    }

    return { authToken: this.jwtService.sign({ email: user.email }) };
  }

  decodeToken(token: string): any {
    return this.jwtService.decode(token.split(' ')[1]);
  }

  associate(pubKey: KeyObject, payload: any): { authToken: string } {
    // tslint:disable-next-line: curly
    if (payload.exp) delete payload.exp;
    payload.pubKey = pubKey;

    return { authToken: this.jwtService.sign(payload) };
  }
}

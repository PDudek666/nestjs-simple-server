import { Module, HttpModule } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { PdfModule } from './pdf/pdf.module';
import { AppController } from './app.controller';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './http.exeption.filter';

@Module({
  providers: [{provide: APP_FILTER, useClass: HttpExceptionFilter}],
  imports: [
      UserModule,
      AuthModule,
      PdfModule,
    ],
  controllers: [AppController],
})
export class AppModule {}

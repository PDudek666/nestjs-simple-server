export const rsa = {
  modulusLength: 4096,
  publicKeyEncoding: { type: 'spki', format: 'pem' },
  privateKeyEncoding: { type: 'pkcs8', format: 'pem', cipher: 'aes-256-cbc', passphrase: 'secret_phrase' },
};

export const pdfUri = 'http://www.africau.edu/images/default/sample.pdf';

export const jwtSecret = 'secret_phrase';

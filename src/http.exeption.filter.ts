
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const parseStatus = (status: number) => status === 401 ? 410 : status;
    const status = parseStatus(exception.getStatus());

    response
      .status(status)
      .json({
        statusCode: status,
      });
  }
}

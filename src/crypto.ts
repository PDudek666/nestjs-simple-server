import { randomBytes, createCipheriv, scryptSync, BinaryLike } from 'crypto';

export const hashBinary = async (bin: BinaryType, password: string): Promise<string> => {
const algorithm = 'aes-192-cbc';
const key = scryptSync(password, randomBytes(24), 24);
const iv = randomBytes(8);

const cipher = createCipheriv(algorithm, key, iv.toString('hex'));

let encrypted = cipher.update(bin, 'binary', 'base64');
encrypted += cipher.final('base64');

return encrypted;
};
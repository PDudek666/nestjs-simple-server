import { Module, HttpModule, HttpService } from '@nestjs/common';
import { PdfService } from './pdf.service';

@Module({
  imports: [HttpModule],
  providers: [PdfService],
  exports: [PdfService],
})
export class PdfModule {}

import { Injectable, HttpService } from '@nestjs/common';
import { pdfUri } from '../config';
import { hashBinary } from '../crypto';

@Injectable()
export class PdfService {
  constructor(
    private readonly httpService: HttpService,
  ) {}

  async getBase64Pdf(publicKey: string): Promise<string> {
    const { data } = await this.httpService.axiosRef({
      url: pdfUri,
      method: 'GET',
    });

    return hashBinary(data, publicKey);
  }
}

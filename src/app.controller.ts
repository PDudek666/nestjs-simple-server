import { Controller, Request, Post, UseGuards, Body, Get, Response, UnauthorizedException, BadRequestException, Catch, UseFilters } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth/auth.service';
import { UserInterface } from './user/user.interface';
import { PdfService } from './pdf/pdf.service';
import { generateKeyPairSync } from 'crypto';
import { rsa } from './config';

@Controller()
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private readonly pdfService: PdfService,
    ) {}

  @Post('api/sign-in')
  async login(@Body() user: UserInterface): Promise<{ authToken: string }> {
    return this.authService.login(user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/api/generate-key-pair')
  async generateKeyPair(@Request() req, @Response() res)  {
    const payload = this.authService.decodeToken(req.headers.authorization);
    const { publicKey, privateKey } = generateKeyPairSync('rsa', rsa);
    const { authToken } = this.authService.associate(publicKey, payload);

    res.set('Authorization', `Bearer ${authToken}`);
    res.send({ pubKey: publicKey, privKey: privateKey });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/api/encrypt')
  async encrypt(@Request() req) {
    const payload = this.authService.decodeToken(req.headers.authorization);
    return this.pdfService.getBase64Pdf(payload.pubKey);
  }
}

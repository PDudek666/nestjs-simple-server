import { Injectable } from '@nestjs/common';
import { UserInterface } from './user.interface';

@Injectable()
export class UserService {
  private readonly users: UserInterface[] = [
    {email: 'example@email.com', password: 'somepassword123'},
    {email: 'example1@email.com', password: 'somepassword123'},
  ];

  find(email: string): UserInterface {
      return this.users.find(u => u.email === email);
  }
}
